#include <string.h>
#include <unistd.h>
#include <openssl/md5.h>
#include <getopt.h>
#include <libcollections/rbtree.h>
#include <libshrewd-cgi/cgi.h>
#include "main.h"
#include "contact.h"


/*
static struct option long_options[] = {
	{ "help",    no_argument,       NULL, 'h' }, // 0
	{ "verbose", no_argument,       NULL, 'v' },
	{ "quiet",   no_argument,       NULL, 'q' },
	{ NULL, 0, NULL, 0 }
};
static char* option_descriptions[] = {
	"This message that you see now.", // 0
	"To enable the printing of extra messages.",
	"To disable the printing of all messages.",
	NULL
};
*/


boolean application_startup  ( void );
boolean application_shutdown ( void );
void    handle_request       ( const char *controller, const char *action );
void    render_navigation    ( void );
void    render_contacts      ( void );
void    render_new_contact   ( void );


#define GUARD_AUTHENTICATED( ) \
	if( !is_authenticated( ) ) \
	{	 \
		cgi_redirect( cgi_url("/main/login") ); \
	}


boolean application_startup( void )
{
	cgi_create( CGI_OPT_ENABLE_LOGGING | CGI_OPT_DISPLAY_ERRORS | CGI_OPT_ENABLE_SESSIONS, 
		APPLICATION_NAME "/" APPLICATION_VERSION, 
		"contact-manager.com"
	);
	rbtree_create( &my_contacts, (rbtree_element_function) contact_destroy, (rbtree_compare_function) contact_compare, malloc, free );

	load_contacts( &my_contacts, CONTACTS_FILENAME );
	
#if 0	
	contact *p_contact = (contact *) malloc( sizeof(contact) );
	memset( p_contact, 0, sizeof(contact) );

	tstrcpy( p_contact->first_name, "Joe" );
	tstrcpy( p_contact->last_name, "Marrero" );
	tstrcpy( p_contact->company_name, "Shrewd LLC" );
	tstrcpy( p_contact->phone1, "(954) 803-9157" );

	rbtree_insert( &my_contacts, p_contact );
#endif

	return TRUE;
}

boolean application_shutdown( void )
{
	//char filename[ 255 ];
	//snprintf( filename, sizeof(filename), "%s/%s", getcwd(NULL,0), CONTACTS_FILENAME );

	save_contacts( &my_contacts, CONTACTS_FILENAME );
	rbtree_destroy( &my_contacts );
	cgi_destroy( );

	return TRUE;
}

int main( int argc, char *argv[] )
{
	if( !application_startup( ) )
	{
		fprintf( stderr, "Unable to startup application.\n" );
		return 1;	
	}

	fcgi_begin( );
		cgi_process_request( );
		cgi_header( "Content-Type: text/html; charset=utf-8" );

		if( argc > 1 && argv[1] )
		{
			/* Use path info from command line if 
 			 * one exists.
 			 */
			cgi_path_info_create( argv[ 1 ] );
		}

		const char *controller = application_controller( );
		const char *action     = application_action( );

		handle_request( controller, action );
		cgi_send_response( );
		cgi_reset( );
	fcgi_end( );

	if( !application_shutdown( ) )
	{
		fprintf( stderr, "Unable to shutdown application.\n" );
		return 2;	
	}

	return 0;
}

boolean is_authenticated( void )
{
	const char *is_auth = cgi_session_get( "is_auth" );

	return is_auth && atoi( is_auth ) == 1;
}

void application_login( void )
{
	boolean is_auth = is_authenticated( );

	if( is_auth )
	{
		cgi_redirect( cgi_url("/main/list") ); 
	}
	else
	{
		const char *username = cgi_post( "username" ); 
		const char *password = cgi_post( "password" ); 

		cgi_out( "<form name='login_form' method='post' enctype='application/x-www-form-urlencoded' >\n" );
			cgi_out( "<label for='username'>Username: </label> \n" );	
			cgi_out( "<input name='username' type='text' /> <br /> \n" );	
	
			cgi_out( "<label for='password'>Password: </label> \n" );	
			cgi_out( "<input name='password' type='password' /> <br />\n" );	
			cgi_out( "<input name='loginbtn' value='Login' type='submit' /> <br />\n" );	
		cgi_out( "</form>" );

		if( username && password )
		{
			char *hashed_password = cgi_md5( password, strlen(password) );

			if( strncasecmp( username, "Joe", 3 ) == 0 && strcmp( HASHED_PASSWORD, hashed_password ) == 0 )
			{
				cgi_session_set( "is_auth", "1" );
				cgi_redirect( cgi_url("main/list") );
			}
			else
			{
				cgi_session_set( "error_msg", "Login failed!" );
				cgi_redirect( cgi_url("main/login") );
			}
		}
	}
}

void application_logout( void )
{
	cgi_session_destroy( );
	cgi_redirectf( cgi_url("main/list") );
}

const char *application_controller( void )
{
	if( cgi.path_info.count > 0 )
	{
		return cgi.path_info.parts[ 0 ];
	}

	return "main";
}

const char *application_action( void )
{
	if( cgi.path_info.count > 1 )
	{
		return cgi.path_info.parts[ 1 ];
	}

	return "index";
}

const char *application_arg( size_t arg )
{
	size_t index = 1 + arg;

	if( cgi.path_info.count > index )
	{
		return cgi.path_info.parts[ index ];
	}

	return NULL;
}


void handle_request( const char *controller, const char *action )
{
	cgi_out( "<!DOCTYPE html>\n" );
	cgi_out( "<html>\n" );
	cgi_out( "<head>\n" );
	cgi_out( "<title> %s </title>\n", cgi_app( ) );
	cgi_out( "</head>\n" );
	cgi_out( "<body>\n" );

	cgi_out( "<p>Controller = %s <br /> \n", controller );
	cgi_out( "Action = %s </p> \n", action );
		
		cgi_out( "Is Auth.: %s <br /> \n", is_authenticated() ? "yes" : "no" );

		{
			render_navigation( );

			if( strcmp( action, "login" ) == 0 )
			{
				application_login( );
			}
			else if( strcmp( action, "logout" ) == 0 )
			{
				application_logout( );
			}
			else if( strcmp( action, "list" ) == 0 )
			{
				GUARD_AUTHENTICATED( );
				render_contacts( );
			}
			else if( strcmp( action, "new" ) == 0 )
			{
				GUARD_AUTHENTICATED( );
				contact *p_contact = (contact *) malloc( sizeof(contact) );	

				if( contact_fill_post( p_contact ) )
				{
					rbtree_insert( &my_contacts, p_contact );
					save_contacts( &my_contacts, CONTACTS_FILENAME );
					cgi_redirect( cgi_url("/main/list") ); 
				}
				else
				{
					free( p_contact );
				}

				render_contact_form( NULL );
			}
			else if( strcmp( action, "edit" ) == 0 )
			{	
				GUARD_AUTHENTICATED( );
				const char *last_name  = application_arg( 1 );
				const char *first_name = application_arg( 2 );
				contact *p_contact     = find_contact_by_name( &my_contacts, last_name, first_name );

				if( contact_fill_post( p_contact ) )
				{
					save_contacts( &my_contacts, CONTACTS_FILENAME );
					cgi_redirect( cgi_url("/main/list") ); 
				}

				render_contact_form( p_contact );
			}
			else if( strcmp( action, "delete" ) == 0 )
			{	
				GUARD_AUTHENTICATED( );
				const char *last_name  = application_arg( 1 );
				const char *first_name = application_arg( 2 );
				contact *p_contact     = find_contact_by_name( &my_contacts, last_name, first_name );

				if( p_contact )
				{
					rbtree_remove( &my_contacts, p_contact );
					save_contacts( &my_contacts, CONTACTS_FILENAME );
					cgi_redirect( cgi_url("/main/list") ); 
				}
			}
			else
			{
				GUARD_AUTHENTICATED( );
				render_contacts( );
			}
		}
	cgi_out( "</body>\n" );
	cgi_out( "</html>\n" );
}

void render_navigation( )
{
	boolean is_auth = is_authenticated( );

	cgi_out( "<ul>" );
		if( is_auth )
		{
			cgi_out( "<li> <a href='%s'> Logout </a> </li>", cgi_url("/main/logout") );
		}
		else
		{
			cgi_out( "<li> <a href='%s'> Login </a> </li>", cgi_url("/main/login") );
		}
		cgi_out( "<li> <a href='%s'> Contacts </a> </li>", cgi_url("/main/list") );
		cgi_out( "<li> <a href='%s'> New Contact </a> </li>", cgi_url("/main/new") );
	cgi_out( "</ul>" );
}

void render_contacts( )
{
	cgi_out( "<h1> My Contacts </h1>\n" );
	list_contacts( &my_contacts );
}


