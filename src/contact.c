#include <stdlib.h>
#include <libcollections/tstring.h>
#include <libutility/utility.h>
#include <libshrewd-cgi/cgi.h>
#include "contact.h"
#include "main.h"

contacts_collection my_contacts;


boolean contact_destroy( contact *p_contact )
{
	free( p_contact );
	return TRUE;
}

int contact_compare( const contact *p_left, const contact *p_right )
{
	int result = tstrcmp( p_left->last_name, p_right->last_name );

	if( result == 0 )
	{
		result = tstrcmp( p_left->first_name, p_right->first_name  );
	}

	return result;
}

void list_contacts( contacts_collection *p_contacts )
{
	if( p_contacts->size > 0 )
	{
		contacts_iterator itr;

		for( itr = rbtree_begin( p_contacts ); 
			 itr != rbtree_end( ); 
			 itr = rbtree_next( itr ) )
		{
			contact *p_contact = (contact *) itr->data;

			cgi_out( "<hr />\n" );
			cgi_out( "(<a href='%s'> X </a>)", cgi_urlf( NULL, NULL, "/main/delete/%s/%s", p_contact->last_name, p_contact->first_name ) );
			cgi_out( "<strong> Name: </strong> <a href='%s'> %s, %s %s </a> <br />\n", 
					cgi_urlf( NULL, NULL, "/main/edit/%s/%s", p_contact->last_name, p_contact->first_name ),
					p_contact->last_name, p_contact->first_name, p_contact->middle_name[0] ? p_contact->middle_name : "" );
			cgi_out( "<strong> Company: </strong> %s <br />\n", p_contact->company_name );
			cgi_out( "<strong> Address: </strong> %s, %s, %s %s <br />\n", p_contact->address1, p_contact->city, p_contact->state, p_contact->postal_code );
			cgi_out( "<strong> Primary Phone: </strong> %s <br />\n", p_contact->phone1 );
			cgi_out( "<strong> Secondary Phone: </strong> %s <br />\n", p_contact->phone2 );
			cgi_out( "<strong> Website: </strong> %s <br />\n", p_contact->website );

		}
		cgi_out( "<hr />\n" );
	}
	else
	{
		cgi_out( "<p> There are no contacts yet. </p>\n" );
	}
}

void render_contact_form( const contact *p_contact )
{
	boolean editing = p_contact != NULL;

	cgi_out( "<h2> %s Contact </h2>\n", editing ? "Edit" : "Create" );


	const char *errors = cgi_cookie_get( "errors" );

	if( errors )
	{
		cgi_out( "<p class='errors'> The following errors have occurred: </p>" );
	}

	if( editing )
	{
		cgi_out( "<form name='edit_contact' method='post' enctype='application/x-www-form-urlencoded' action='%s'>\n", 
				cgi_urlf(NULL, NULL, "%s/%s/%s/%s", application_controller(), application_action(), application_arg(1), application_arg(2)) );
	}
	else
	{
		cgi_out( "<form name='create_contact' method='post' enctype='application/x-www-form-urlencoded' action='%s'>\n", 
				cgi_urlf(NULL, NULL, "%s/%s", application_controller(), application_action()) );
	}
		cgi_out( "<table>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='first_name'> First Name: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='first_name' type='text' value='%s' /> <br />\n", p_contact ? p_contact->first_name : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='middle_name'> Middle Name: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='middle_name' type='text' value='%s' /> <br />\n", p_contact ? p_contact->middle_name : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='last_name'> Last Name: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='last_name' type='text' value='%s' /> <br />\n", p_contact ? p_contact->last_name : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='company_name'> Company: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='company_name' type='text' value='%s' /> <br />\n", p_contact ? p_contact->company_name : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='phone1'> Primary Phone: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='phone1' type='phone' value='%s' /> <br />\n", p_contact ? p_contact->phone1 : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='phone2'> Secondary Phone: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='phone2' type='phone' value='%s' /> <br />\n", p_contact ? p_contact->phone2 : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );



			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='address1'> Address Line 1: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='address1' type='text' value='%s' /> <br />\n", p_contact ? p_contact->address1 : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='address2'> Address Line 2: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='address2' type='text' value='%s' /> <br />\n", p_contact ? p_contact->address2 : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='city'> City: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='city' type='text' value='%s' /> <br />\n", p_contact ? p_contact->city : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='state'> State: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='state' type='text' value='%s' /> <br />\n", p_contact ? p_contact->state : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='postal_code'> Postal Code: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='postal_code' type='text' value='%s' /> <br />\n", p_contact ? p_contact->postal_code : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );
			cgi_out( "<tr>\n" );
				cgi_out( "<td align='right'>\n" );
					cgi_out( "<label for='website'> Website: </label> &nbsp;\n" );
				cgi_out( "</td>\n" );
				cgi_out( "<td>\n" );
					cgi_out( "<input name='website' type='url' value='%s' /> <br />\n", p_contact ? p_contact->website : "" );
				cgi_out( "</td>\n" );
			cgi_out( "</tr>\n" );

		cgi_out( "</table>\n" );
		
		cgi_out( "<input name='save_btn' type='submit' value='Save' /> <br />\n" );
	cgi_out( "</form>\n" );
}

boolean contact_fill_post( contact *p_contact )
{
	boolean modified = FALSE;

	POST_FILL( p_contact, first_name );
	POST_FILL( p_contact, last_name );
	POST_FILL( p_contact, middle_name );
	POST_FILL( p_contact, company_name );
	POST_FILL( p_contact, address1 );
	POST_FILL( p_contact, address2 );
	POST_FILL( p_contact, city );
	POST_FILL( p_contact, state );
	POST_FILL( p_contact, postal_code );
	POST_FILL( p_contact, phone1 );
	POST_FILL( p_contact, phone2 );
	POST_FILL( p_contact, website );

	return modified;
}

contact* find_contact_by_name ( contacts_collection *p_contacts, const char *last_name, const char *first_name )
{
	contacts_iterator itr;

	for( itr = rbtree_begin( p_contacts ); itr != rbtree_end( ); itr = rbtree_next(itr) )
	{
		contact *p_contact = (contact *) itr->data;
		boolean found = tstrcasecmp( p_contact->last_name, last_name ? last_name : "" ) == 0 && 
		                tstrcasecmp( p_contact->first_name, first_name ? first_name : "" ) == 0;

		if( found )
		{
			return p_contact;
		}
	}

	return NULL;
}

boolean load_contacts( contacts_collection *p_contacts, const char *filename )
{
	boolean result = FALSE;

	if( !p_contacts )
	{
		return FALSE;
	}

	if( file_size(filename) > 0 )
	{
		FILE *file     = fopen( filename, "rb" );

		if( file )
		{
			result = rbtree_unserialize( p_contacts, sizeof(contact), file );

			fclose( file );
		}
	}
	else
	{
		// save an empty contacts file.
		result = save_contacts( p_contacts, filename );
	}

	return result;
}

boolean save_contacts( contacts_collection *p_contacts, const char *filename )
{
	if( !p_contacts )
	{
		return FALSE;
	}

	FILE *file = fopen( filename, "wb+" );
	boolean result = FALSE;

	if( file )
	{	
		result = rbtree_serialize( p_contacts, sizeof(contact), file );
	}

	fclose( file );

	return result;
}
