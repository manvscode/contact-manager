#ifndef _CONTACT_H_
#define _CONTACT_H_

#include <time.h>
#include <libcollections/tstring.h>
#include <libcollections/rbtree.h>


typedef rbtree_t          contacts_collection;
typedef rbtree_iterator_t contacts_iterator;

#pragma pack(push, 1)
typedef struct _contact {
	tchar first_name[ 50 ];
	tchar last_name[ 50 ];
	tchar middle_name[ 50 ];

	tchar company_name[ 120 ];

	tchar address1[ 80 ];
	tchar address2[ 80 ];
	tchar city[ 50 ];
	tchar state[ 50 ];
	tchar postal_code[ 12 ];

	tchar phone1[ 16 ];
	tchar phone2[ 16 ];

	tchar website[ 256 ];

	time_t date_of_birth;
} contact;
#pragma pack(pop)



extern contacts_collection my_contacts;

void    list_contacts       ( contacts_collection *p_contacts );
void    render_contact_form ( const contact *p_contact );
boolean contact_fill_post   ( contact *p_contact );

contact* find_contact_by_name ( contacts_collection *p_contacts, const char *last_name, const char *first_name );
boolean load_contacts   ( contacts_collection *p_contacts, const char *filename );
boolean save_contacts   ( contacts_collection *p_contacts, const char *filename );
boolean contact_destroy ( contact *p_contact );
int     contact_compare ( const contact *p_left, const contact *p_right );

#endif /* _CONTACT_H_ */
