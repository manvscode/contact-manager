#ifndef _MAIN_H_
#define _MAIN_H_

#include <time.h>

#define APPLICATION_NAME                    "contact-list"
#define APPLICATION_VERSION                 "0.1"
#define CONTACTS_FILENAME                   "/home/joe/projects/contact-manager/src/contacts.dat"
#define HASHED_PASSWORD                     "b9898b54c184477a44a5d745ef0804b8"

const char *application_controller  ( void );
const char *application_action      ( void );
const char *application_arg         ( size_t arg );

#endif /* _MAIN_H_ */
